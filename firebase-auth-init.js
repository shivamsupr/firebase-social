/*************************** Firebase Auth Init ******************************/

SZJ.prototype.initFirebaseSocialConfig = function() {
    $.getScript("https://www.gstatic.com/firebasejs/4.2.0/firebase-app.js", function() {
        $.getScript("https://www.gstatic.com/firebasejs/4.2.0/firebase-auth.js", function() {
            // Initialize Firebase
            var config = {
                apiKey: "AIzaSyDIDav1YATIGA5MVRh-moeWup35Qy01E4c",
                authDomain: "usrauth.firebaseapp.com",
                databaseURL: "https://usrauth.firebaseio.com",
                projectId: "usrauth",
                storageBucket: "",
                messagingSenderId: "796858569638"
            };
            firebase.initializeApp(config);

            $('.socialBtns').removeClass('hide');
        });
    });
};
