function FirebaseSocial() {

}

var FBSL = new FirebaseSocial();

FirebaseSocial.prototype.handleFacebookSignIn = function (onSuccess, onError) {
    if (!firebase.auth().currentUser) {
        // initialize facebook auth provider
        var provider = new firebase.auth.FacebookAuthProvider();
        // add scopes
        provider.addScope('user_birthday');
        this.handleSignInCallback(provider, onSuccess, onError);
    }
};

FirebaseSocial.prototype.handleGoogleSignIn = function (onSuccess, onError) {
    if (!firebase.auth().currentUser) {
        // initialize google auth provider
        var provider = new firebase.auth.GoogleAuthProvider();
        // add scopes
        provider.addScope('https://www.googleapis.com/auth/contacts.readonly');
        this.handleSignInCallback(provider, onSuccess, onError);
    }
};

FirebaseSocial.prototype.handleTwitterSignIn = function (onSuccess, onError) {
    if (!firebase.auth().currentUser) {
        // initialize twitter auth provider
        var provider = new firebase.auth.TwitterAuthProvider();

        this.handleSignInCallback(provider, onSuccess, onError);
    }
};

FirebaseSocial.prototype.handleSignOut = function () {
    firebase.auth().signOut();
};

FirebaseSocial.prototype.handleSignInCallback = function (provider, onSuccess, onError) {
    // init sign-in
    firebase.auth().signInWithPopup(provider).then(function (result) {
        if (onSuccess !== undefined) {
            onSuccess(result, JSON.parse(JSON.stringify(result)));
        }
    }).catch(function (error) {
        if (onError !== undefined) {
            onError(error);
        }
    });
};

